package com.budget.mysavings.models;

import lombok.Data;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
@Table(name="`user`")
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID uuid;
    private String lastname;
    private String firstname;
    private String email;
    private String password;
    private String passwordConfirm;
}

