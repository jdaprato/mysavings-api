package com.budget.mysavings.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Data
@Entity
public class Category {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    public UUID uuid;
    public String name;
    @OneToMany(mappedBy = "category")
    private List<BudgetItem> budgetItems;

}
