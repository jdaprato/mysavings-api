package com.budget.mysavings.models;

import com.budget.mysavings.enums.ItemType;
import com.budget.mysavings.enums.PaymentMean;
import lombok.Data;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Entity
public class BudgetItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private UUID uuid;

    private String name;
    private Float amount;
    private Boolean recurrence;
    private LocalDateTime recurrenceDate;
    @Enumerated(EnumType.STRING)
    private ItemType type;
    @Enumerated(EnumType.STRING)
    private PaymentMean paymentMean;
    private String comment;
    @ManyToOne
    @JoinColumn(name="category_id")
    private Category category;

}
