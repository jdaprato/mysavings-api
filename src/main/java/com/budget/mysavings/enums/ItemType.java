package com.budget.mysavings.enums;

public enum ItemType {
    INCOME {
        @Override
        public boolean isIncome() {
            return true;
        }
    },
    EXPENSE {
        @Override
        public boolean isExpense() {
            return true;
        }
    };

    public boolean isIncome() { return false;}
    public boolean isExpense() { return false;}
}
