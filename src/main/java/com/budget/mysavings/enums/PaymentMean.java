package com.budget.mysavings.enums;

public enum PaymentMean {
        CREDIT_CARD {
            @Override
            public boolean isCC() {
                return true;
            }
        },
        CREDIT_TRANSFERT {
            @Override
            public boolean isCT(){
                return true;
            }
        },
        CHEQUE {
            @Override
            public boolean isCheque(){
                return true;
            }
        },
        CASH {
            @Override
            public boolean isCash(){
                return true;
            }
        },
        CASH_WITHDRAWAL {
            @Override
            public boolean isCW() {
                return true;
            }
        },
        DRAWDOWN {
            @Override
            public boolean isDrawdown(){
                return true;
            }
        };

        public boolean isCC() {return false;}
        public boolean isCT() {return false;}
        public boolean isCheque() {return false;}
        public boolean isCash() {return false;}
        public boolean isCW() {return false;}
        public boolean isDrawdown() {return false;}

}
