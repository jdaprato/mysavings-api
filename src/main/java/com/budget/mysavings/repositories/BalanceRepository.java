package com.budget.mysavings.repositories;

import com.budget.mysavings.models.Balance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

public interface BalanceRepository extends CrudRepository<Balance, UUID> {
    List<Balance>findByDate(@Param("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date);
}
