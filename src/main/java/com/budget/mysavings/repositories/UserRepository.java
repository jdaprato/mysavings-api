package com.budget.mysavings.repositories;

import com.budget.mysavings.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;
@RepositoryRestResource(path="users")
public interface UserRepository extends CrudRepository<User, UUID> {

}