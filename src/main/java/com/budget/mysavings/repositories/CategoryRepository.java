package com.budget.mysavings.repositories;

import com.budget.mysavings.models.Category;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(path="categories")
public interface CategoryRepository extends PagingAndSortingRepository<Category, UUID> {
}
