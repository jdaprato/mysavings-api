package com.budget.mysavings.repositories;

import com.budget.mysavings.enums.ItemType;
import com.budget.mysavings.enums.PaymentMean;
import com.budget.mysavings.models.BudgetItem;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;
import java.util.UUID;

@RepositoryRestResource(path="budgetItems")
public interface BudgetItemRepository extends PagingAndSortingRepository<BudgetItem, UUID> {
    List<BudgetItem> findByName(@Param("name") String name);
    List<BudgetItem> findByType(@Param("type")ItemType type);
    List<BudgetItem> findByPaymentMean(@Param("paymentMean")PaymentMean paymentMean);
}
