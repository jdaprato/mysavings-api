package com.budget.mysavings;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MysavingsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MysavingsApplication.class, args);
	}

}
